<?php

namespace Application\Traits;

use \Psr\Http\Message\ResponseInterface as Response;

trait HandlebarsLayoutView
{
    /**
     * The name of the layout the controller will render
     *
     * @var  string
     */
    protected $layout = 'default';

    /**
     * Renders provided data into a view
     *
     * @param   \Slim\Views\Lightncandy  $renderer  Lightncandy Handlebars renderer
     * @param   Response                 $response  HTTP Response to write HTML to
     * @param   string                   $viewFile  Name of view file to use in rendering
     * @param   array                    $data      Associative array of data to use in rendering
     *
     * @return  Response                            HTTP Response with HTML body
     */
    protected function render(
        \Slim\Views\Lightncandy $renderer,
        Response $response,
        string $viewFile,
        array $data = array()
    )
    {
        $view = $this->renderFile($renderer, 'views' . DIRECTORY_SEPARATOR . $viewFile, $data);
        $layout = $this->renderFile(
            $renderer,
            'layouts' . DIRECTORY_SEPARATOR . $this->layout,
            array_merge(
                $data,
                array('__content' => $view)
            )
        );

        $response->getBody()->write($layout);

        return $response;
    }

    /**
     * Wraps the Lightncandy render method which echoes instead of returning
     *
     * @param   \Slim\Views\Lightncandy  $renderer  Lightncandy Handlebars renderer
     * @param   string                   $file      File to render
     * @param   array                    $data      Data to render with
     *
     * @return  string                              HTML response body
     */
    private function renderFile(
        \Slim\Views\Lightncandy $renderer,
        string $file,
        array $data = array()
    )
    {
        ob_start();
        $renderer->render($file, $data);

        return ob_get_clean();
    }
}
