<?php

namespace Application;

use \Application\Controllers;
use \Dandelionmood\LastFm\LastFm;
use \LightnCandy;
use \PopularArtists\Searchers;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

class Bootstrap
{
    public function getApplication()
    {
        $config['displayErrorDetails'] = true;
        $config['addContentLengthHeader'] = false;

        $app = new \Slim\App(new \Slim\Container(array('settings' => $config)));
        $container = $app->getContainer();

        $libLastFm = new LastFm(
            getenv('LASTFM_API_KEY'),
            getenv('LASTFM_SECRET_KEY')
        );
        $searcher = new Searchers\LastFm($libLastFm);

        $indexController = new Controllers\Index($searcher);

        $container['view'] = function ($container) {
            return new \Slim\Views\Lightncandy('../templates', array(
                'flags' => LightnCandy\Flags::FLAG_HANDLEBARSJS |
                           LightnCandy\Flags::FLAG_ERROR_EXCEPTION |
                           LightnCandy\Flags::FLAG_ERROR_LOG |
                           LightnCandy\Flags::FLAG_RUNTIMEPARTIAL,
            ));
        };

        $app->get('/', function(Request $request, Response $response) use ($indexController) {
            return $indexController->index($this, $request, $response);
        });

        return $app;
    }
}
