<?php

namespace Application\Controllers;

use \Application\Traits;
use \PopularArtists\Searcher;
use \PopularArtists\Exception;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Zend\Paginator\Adapter;
use \Zend\Paginator\Paginator;

class Index
{
    use Traits\HandlebarsLayoutView;

    /**
     * How many artists to display per page
     */
    const RESULTS_PER_PAGE = 5;

    /**
     * A Searcher is used to find popular artists by country name
     *
     * @var  Searcher
     */
    private $searcher;

    /**
     * Constructs the Index controller
     *
     * @param  Searcher  $searcher  @see self::$searcher
     */
    public function __construct(Searcher $searcher)
    {
        $this->searcher = $searcher;
    }

    /**
     * Index route handles the display and search of popular artists
     *
     * @param   \Slim\Container  $container  Slim container context
     * @param   Request          $request    HTTP Request
     * @param   Response         $response   HTTP Response
     *
     * @return  Response                     HTTP Response
     */
    public function index(\Slim\Container $container, Request $request, Response $response)
    {
        $params = $request->getQueryParams();

        $artists = array();
        $country = false;
        $error = false;
        $pagination = array();

        if (!empty($params['country'])) {
            $country = $params['country'];
            $pageNumber = !empty($params['page']) && (int)$params['page'] > 0 ? (int)$params['page'] : 1;

            try {
                $artistsCollection = $this->searcher->searchPopularArtistsByCountry(
                    $params['country'],
                    $pageNumber,
                    self::RESULTS_PER_PAGE
                );
                $artists = $artistsCollection->getArtistsAsArrays();

                $paginator = new Paginator(new Adapter\NullFill($artistsCollection->getTotalResults()));
                $paginator->setCurrentPageNumber($artistsCollection->getPageNumber());

                $pagination = (array)$paginator->getPages();
            } catch (Exception\InvalidCountry $e) {
                $error = sprintf('"%s" is an invalid country. Perhaps try another?', $country);
            }
        }

        return $this->render($container['view'], $response, 'index/index', array(
            'artists' => $artists,
            'country' => $country,
            'error' => $error,
            'pagination' => $pagination,
        ));
    }
}
