<?php

use \Application\Bootstrap;

require '../vendor/autoload.php';

$bootstrap = new Bootstrap;

$app = $bootstrap->getApplication();
$app->run();
