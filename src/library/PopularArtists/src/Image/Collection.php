<?php

namespace PopularArtists\Image;

class Collection
{
    /**
     * The Images this Collection holds
     *
     * @var  []Image
     */
    private $images = array();

    /**
     * Constructs the Collection
     *
     * @param  []Image  $images  @see self::$images
     */
    public function __construct(Image ...$images)
    {
        $this->images = $images;
    }

    /**
     * Returns the Images
     *
     * @return  @see self::$images
     */
    public function getImages(): array
    {
        return $this->images;
    }

    /**
     * Returns the Images in this Collection as their data arrays
     *
     * @return  array
     */
    public function getImagesAsArrays(): array
    {
        $images = array();

        foreach ($this->images as $image) {
            $data = $image->toArray();
            $images[$data[Image::SIZE]] = $data;
        }

        return $images;
    }
}
