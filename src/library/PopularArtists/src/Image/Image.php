<?php

namespace PopularArtists\Image;

class Image
{
    /**
     * Property references
     */

    const TEXT = 'text';
    const SIZE = 'size';

    /**
     * Size references
     */

    const SIZE_SMALL = 'small';
    const SIZE_MEDIUM = 'medium';
    const SIZE_LARGE = 'large';
    const SIZE_EXTRALARGE = 'extralarge';
    const SIZE_MEGA = 'mega';

    /**
     * The URL to the Image
     *
     * @todo  Rename to URL even though the last.fm returns as "text"
     *
     * @var  string
     */
    private $text;

    /**
     * The Size of the Image
     *
     * @see   Size references constants
     * @todo  Add validation around setting this Size
     *
     * @var  string
     */
    private $size;

    /**
     * Constructs an Image
     *
     * @param  string  $text  @see self::$text
     * @param  string  $size  @see self::$size
     */
    public function __construct(
        string $text,
        string $size
    )
    {
        $this->text = $text;
        $this->size = $size;
    }

    /**
     * Returns the Text
     *
     * @return  @see self::$text
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * Returns the Size
     *
     * @return  @see self::$size
     */
    public function getSize(): string
    {
        return $this->size;
    }

    /**
     * Returns whether this Image is the given size
     *
     * @param   string   $size  Size to check against. @see Size references constants
     *
     * @return  boolean         Whether this Image is the given size
     */
    public function isSize(string $size): bool
    {
        return $this->getSize() === $size;
    }

    /**
     * Returns the data of the Image as an associative array
     *
     * @return  array
     */
    public function toArray(): array
    {
        return array(
            self::TEXT => $this->getText(),
            self::SIZE => $this->getSize(),
        );
    }
}
