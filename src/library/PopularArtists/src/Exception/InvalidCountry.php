<?php

namespace PopularArtists\Exception;

class InvalidCountry extends \Exception
{
    /**
     * The country that was invalid
     *
     * @var  string
     */
    private $country;

    /**
     * Construct the InvalidCountry exception
     *
     * @param  string  $country  @see self::$country
     */
    public function __construct(string $country)
    {
        $this->country = $country;

        parent::__construct("Invalid country: '{$country}'", 6);
    }

    /**
     * Returns the Country that was invalid
     *
     * @return  @see self::$country
     */
    public function getCountry(): string
    {
        return $this->country;
    }
}
