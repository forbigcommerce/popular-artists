<?php

namespace PopularArtists\Searchers;

use \Dandelionmood\LastFm\LastFm as LibLastFm;
use \PopularArtists\Artist;
use \PopularArtists\Exception;
use \PopularArtists\Image;
use \PopularArtists\Searcher;

class LastFm implements Searcher
{
    /**
     * The library to query the last.fm api with
     *
     * @var  LibLastFm
     */
    private $lib;

    /**
     * Constructs the LastFm searcher
     *
     * @param  LibLastFm  $libLastFm  @see self::$lib
     */
    public function __construct(LibLastFm $libLastFm)
    {
        $this->lib = $libLastFm;
    }

    /**
     * searchPopularArtistsByCountry searches for the most popular artists in a
     * given country.
     *
     * @param   string  $country   Country name as defined by the ISO 3166-1 standard
     * @param   int     $page      The page of results to retrieve
     * @param   int     $perPage   How many results to retrieve
     *
     * @throws  Exception\InvalidCountry
     * @throws  \Exception
     *
     * @return  Artist\Collection  A collection of Artists
     */
    public function searchPopularArtistsByCountry(string $country, int $page, int $perPage): Artist\Collection
    {
        try {
            $topArtists = $this->lib->geo_gettopartists(array(
                'country' => $country,
                'limit' => $perPage,
                'page' => $page,
            ));
        } catch (\Exception $e) {
            $message = $e->getMessage();

            switch (true) {
                case strpos($message, '6|country param invalid') !== false:
                    throw new Exception\InvalidCountry($country);
                    break;
                default:
                    throw $e;
            }
        }

        $artists = array();
        $totalResults = 0;

        if ($topArtists->topartists) {
            $totalResults = $topArtists->topartists->{'@attr'}->total;

            foreach ($topArtists->topartists->artist as $topArtist) {
                $images = array();

                foreach ($topArtist->image as $topArtistImage) {
                    $image = new Image\Image(
                        $topArtistImage->{'#text'},
                        $topArtistImage->size
                    );

                    $images[] = $image;
                }

                $imageCollection = new Image\Collection(...$images);

                $artist = new Artist\Artist(
                    $topArtist->name,
                    (int)$topArtist->listeners,
                    $topArtist->mbid,
                    $topArtist->url,
                    (bool)$topArtist->streamable,
                    $imageCollection
                );

                $artists[] = $artist;
            }
        }

        $collection = new Artist\Collection($totalResults, $page, ...$artists);

        return $collection;
    }
}
