<?php

namespace PopularArtists;

use PopularArtists\Artist;

interface Searcher
{
    /**
     * searchPopularArtistsByCountry searches for the most popular artists in a
     * given country.
     *
     * @param   string  $country   Country name
     * @param   int     $page      The page of results to retrieve
     * @param   int     $perPage   How many results to retrieve
     *
     * @throws  Exception\InvalidCountry
     *
     * @return  Artist\Collection  A collection of Artists
     */
    public function searchPopularArtistsByCountry(
        string $country,
        int $page,
        int $perPage
    ): Artist\Collection;
}
