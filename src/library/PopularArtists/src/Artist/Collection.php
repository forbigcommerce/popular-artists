<?php

namespace PopularArtists\Artist;

class Collection
{
    /**
     * The total number of results beyond this Collection
     *
     * @var  int
     */
    private $totalResults;

    /**
     * The page number this Collection represents
     *
     * @var  int
     */
    private $pageNumber;

    /**
     * The Artists this Collection holds
     *
     * @var  []Artist
     */
    private $artists = array();

    /**
     * Constructs the Collection
     *
     * @param  int       $totalResults  @see self::$totalResults
     * @param  int       $pageNumber    @see self::$pageNumber
     * @param  []Artist  $artists       @see self::$artists
     */
    public function __construct(int $totalResults, int $pageNumber, Artist ...$artists)
    {
        $this->totalResults = $totalResults;
        $this->pageNumber = $pageNumber;
        $this->artists = $artists;
    }

    /**
     * Returns the Total Results
     *
     * @return  @see self::$totalResults
     */
    public function getTotalResults(): int
    {
        return $this->totalResults;
    }

    /**
     * Returns the Page Number
     *
     * @return  @see self::$pageNumber
     */
    public function getPageNumber(): int
    {
        return $this->pageNumber;
    }

    /**
     * Returns the Artists
     *
     * @return  @see self::$artists
     */
    public function getArtists(): array
    {
        return $this->artists;
    }

    /**
     * Returns the Artists in this Collection as their data arrays
     *
     * @return  array
     */
    public function getArtistsAsArrays(): array
    {
        $artists = array();

        foreach ($this->artists as $artist) {
            $artists[] = $artist->toArray();
        }

        return $artists;
    }
}
