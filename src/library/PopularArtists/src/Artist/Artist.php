<?php

namespace PopularArtists\Artist;

use \PopularArtists\Image;

class Artist
{
    /**
     * Property references
     */

    const NAME = 'name';
    const LISTENERS = 'listeners';
    const MBID = 'mbid';
    const URL = 'url';
    const STREAMABLE = 'streamable';
    const IMAGES = 'images';

    /**
     * Name of the Artist
     *
     * @var  string
     */
    private $name;

    /**
     * How many listeners the Artist has
     *
     * @var  int
     */
    private $listeners;

    /**
     * MBID of Artist
     *
     * @var  string
     */
    private $mbid;

    /**
     * URL to Artist on last.fm
     *
     * @var  string
     */
    private $url;

    /**
     * Whether the Artist is streamable
     *
     * @var  boolean
     */
    private $streamable;

    /**
     * The images that represent the Artist
     *
     * @var  Image\Collection
     */
    private $images;

    /**
     * Constructs an Artist
     *
     * @param  string            $name        @see self::$name
     * @param  int               $listeners   @see self::$listeners
     * @param  string            $mbid        @see self::$mbid
     * @param  string            $url         @see self::$url
     * @param  bool              $streamable  @see self::$streamable
     * @param  Image\Collection  $images      @see self::$images
     */
    public function __construct(
        string $name,
        int $listeners,
        string $mbid,
        string $url,
        bool $streamable,
        Image\Collection $images
    ) {
        $this->name = $name;
        $this->listeners = $listeners;
        $this->mbid = $mbid;
        $this->url = $url;
        $this->streamable = $streamable;
        $this->images = $images;
    }

    /**
     * Returns the name of the Artist
     *
     * @return  @see self::$name
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Returns the listeners of the Artist
     *
     * @return  @see self::$listeners
     */
    public function getListeners(): int
    {
        return $this->listeners;
    }

    /**
     * Returns the mbid of the Artist
     *
     * @return  @see self::$mbid
     */
    public function getMbid(): string
    {
        return $this->mbid;
    }

    /**
     * Returns the url of the Artist
     *
     * @return  @see self::$url]
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * Returns the streamable of the Artist
     *
     * @return  @see self::$streamable
     */
    public function getStreamable(): bool
    {
        return $this->streamable;
    }

    /**
     * Returns the images of the Artist
     *
     * @return  @see self::$images
     */
    public function getImages(): Image\Collection
    {
        return $this->images;
    }

    /**
     * Returns the data of the Artist as an associative array
     *
     * @return  array
     */
    public function toArray(): array
    {
        $imagesCollection = $this->getImages();
        $images = $imagesCollection->getImagesAsArrays();

        return array(
            self::NAME => $this->getName(),
            self::LISTENERS => $this->getListeners(),
            self::MBID => $this->getMbid(),
            self::URL => $this->getUrl(),
            self::STREAMABLE => $this->getStreamable(),
            self::IMAGES => $images,
        );
    }
}
