<?php

namespace PopularArtists\Tests\Traits;

use \PopularArtists\Artist;
use \PopularArtists\Image;
use \PopularArtists\Tests\Traits;

trait ArtistData
{
    use Traits\ImageData;

    private function getArtistData(array $override = array())
    {
        $data = array(
            Artist\Artist::NAME => 'Radiohead',
            Artist\Artist::LISTENERS => 3452352,
            Artist\Artist::MBID => 'abcd-ghjkl-ljil',
            Artist\Artist::URL => 'http://last.fm/radiohead',
            Artist\Artist::STREAMABLE => false,
            Artist\Artist::IMAGES => new Image\Collection(...array(
                $this->getImage($this->getImageData()),
                $this->getImage($this->getImageData(array(
                    Image\Image::SIZE => Image\Image::SIZE_MEDIUM,
                ))),
            )),
        );

        $data = array_merge($data, $override);

        return $data;
    }

    private function getArtist(array $data = array())
    {
        return new Artist\Artist(
            $data[Artist\Artist::NAME],
            $data[Artist\Artist::LISTENERS],
            $data[Artist\Artist::MBID],
            $data[Artist\Artist::URL],
            $data[Artist\Artist::STREAMABLE],
            $data[Artist\Artist::IMAGES]
        );
    }
}
