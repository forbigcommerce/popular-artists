<?php

namespace PopularArtists\Tests\Traits;

use \PopularArtists\Image\Image;

trait ImageData
{
    private function getImageData(array $override = array())
    {
        $data = array(
            Image::TEXT => 'http://example.com/image.png',
            Image::SIZE => Image::SIZE_SMALL,
        );

        $data = array_merge($data, $override);

        return $data;
    }

    private function getImage(array $data = array())
    {
        return new Image(
            $data[Image::TEXT],
            $data[Image::SIZE]
        );
    }
}
