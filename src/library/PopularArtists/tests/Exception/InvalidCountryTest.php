<?php

namespace PopularArtists\Tests\Exception;

use \PHPUnit\Framework\TestCase;
use \PopularArtists\Exception\InvalidCountry;

class InvalidCountryTest extends TestCase
{
    /**
     * @covers \PopularArtists\Exception\InvalidCountry::__construct
     */
    public function testConstructor()
    {
        $exception = new InvalidCountry('foo');

        $this->assertInstanceOf(InvalidCountry::class, $exception);
    }

    /**
     * @covers \PopularArtists\Exception\InvalidCountry::getCountry
     */
    public function testGetCountry()
    {
        $country = 'invalidCountry';
        $exception = new InvalidCountry($country);

        $this->assertEquals($country, $exception->getCountry());
    }
}
