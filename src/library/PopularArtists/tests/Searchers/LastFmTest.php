<?php

namespace PopularArtists\Tests\Searchers;

use \Dandelionmood\LastFm\LastFm as LibLastFm;
use \PHPUnit\Framework\TestCase;
use \PopularArtists\Searchers\LastFm;
use \PopularArtists\Exception;

class LastFmTest extends TestCase
{
    private function getMockLib()
    {
        $mock = $this->getMockBuilder(LibLastFm::class)
                     ->disableOriginalConstructor()
                     ->setMethods(
                        array('geo_gettopartists')
                     )
                     ->getMock();

        return $mock;
    }

    /**
     * @covers \PopularArtists\Searchers\LastFm::__construct
     */
    public function testConstructor()
    {
        $searcher = new LastFm($this->getMockLib());

        $this->assertInstanceOf(LastFm::class, $searcher);
    }

    public function searchPopularArtistsByCountryProvider()
    {
        $libMultipleWith = array(
            'country' => 'australia',
            'limit' => 5,
            'page' => 1,
        );

        $libMultipleResults = (object)array(
            'topartists' => (object)array(
                'artist' => array(
                    (object)array(
                        'name' => 'Radiohead',
                        'listeners' => '4505286',
                        'mbid' => 'a74b1b7f-71a5-4011-9441-d0b5e4122711',
                        'url' => 'https://www.last.fm/music/Radiohead',
                        'streamable' => '0',
                        'image' => array(
                            (object)array(
                                '#text' => 'https://lastfm-img2.akamaized.net/i/u/34s/345.png',
                                'size' => 'small'
                            ),
                            (object)array(
                                '#text' => 'https://lastfm-img2.akamaized.net/i/u/34s/346.png',
                                'size' => 'medium'
                            ),
                        ),
                    ),
                    (object)array(
                        'name' => 'David Bowie',
                        'listeners' => '3117123',
                        'mbid' => '5441c29d-3602-4898-b1a1-b77fa23b8e50',
                        'url' => 'https://www.last.fm/music/David+Bowie',
                        'streamable' => '0',
                        'image' => (object)array(),
                    ),
                ),
                '@attr' => (object)array(
                    'total' => '12345',
                ),
            ),
        );

        return array(
            'Results' => array(
                $libMultipleWith,
                $libMultipleResults,
            ),
        );
    }

    /**
     * @dataProvider searchPopularArtistsByCountryProvider
     * @covers \PopularArtists\Searchers\LastFm::searchPopularArtistsByCountry
     */
    public function testSearchPopularArtistsByCountr
    (
        array $libWith,
        \stdClass $libResult
    ) {
        $mock = $this->getMockLib();

        $mock->method('geo_gettopartists')
             ->with($libWith)
             ->willReturn($libResult);

        $searcher = new LastFm($mock);
        $collection = $searcher->searchPopularArtistsByCountry(
            $libWith['country'],
            $libWith['page'],
            $libWith['limit']
        );

        $this->assertEquals($libResult->topartists->{'@attr'}->total, $collection->getTotalResults());
        $this->assertEquals($libWith['page'], $collection->getPageNumber());

        $actualArtists = $collection->getArtists();

        $this->assertEquals(count($libResult->topartists->artist), count($actualArtists));

        foreach ($libResult->topartists->artist as $index => $topArtist) {
            $this->assertEquals($topArtist->name, $actualArtists[$index]->getName());
            $this->assertEquals($topArtist->listeners, $actualArtists[$index]->getListeners());
            $this->assertEquals($topArtist->mbid, $actualArtists[$index]->getMbid());
            $this->assertEquals($topArtist->url, $actualArtists[$index]->getUrl());
            $this->assertEquals((bool)$topArtist->streamable, $actualArtists[$index]->getStreamable());
        }
    }

    public function searchPopularArtistsByCountryThrowsProperExceptionsProvider()
    {
        $unknownException = new \Exception('[xxx|blah]');

        return array(
            'Invalid Country' => array(
                new \Exception('[6|country param invalid] method=geo...'),
                new Exception\InvalidCountry('invalid'),
            ),
            'Unknown' => array(
                $unknownException,
                $unknownException,
            ),
        );
    }

    /**
     * @dataProvider searchPopularArtistsByCountryThrowsProperExceptionsProvider
     * @covers \PopularArtists\Searchers\LastFm::searchPopularArtistsByCountry
     */
    public function testSearchPopularArtistsByCountryThrowsProperExceptions($throw, $expected)
    {
        $mock = $this->getMockLib();

        $mock->method('geo_gettopartists')
             ->will($this->throwException($throw));

        $searcher = new LastFm($mock);

        $thrown = false;

        try {
            $collection = $searcher->searchPopularArtistsByCountry(
                'invalid',
                1,
                5
            );
        } catch (\Exception $actual) {
            $this->assertEquals($expected, $actual);
            $thrown = true;
        }

        $this->assertTrue($thrown);
    }
}
