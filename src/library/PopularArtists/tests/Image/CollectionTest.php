<?php

namespace PopularArtists\Tests\Image;

use \PHPUnit\Framework\TestCase;
use \PopularArtists\Image\Collection;
use \PopularArtists\Tests\Traits;

class CollectionTest extends TestCase
{
    use Traits\ImageData;

    private $collection;
    private $collectionVariadicArray;
    private $images;

    public function setUp()
    {
        $image = $this->getImage($this->getImageData());
        $image2 = $this->getImage($this->getImageData());

        $this->images = array($image, $image2);
        $this->collection = new Collection($image, $image2);
        $this->collectionVariadicArray = new Collection(...$this->images);
    }

    /**
     * @covers \PopularArtists\Image\Collection::__construct
     */
    public function testConstructor()
    {
        $this->assertInstanceOf(Collection::class, $this->collection);
        $this->assertInstanceOf(Collection::class, $this->collectionVariadicArray);
    }

    /**
     * @covers \PopularArtists\Image\Collection::getImages
     */
    public function testGetImages()
    {
        $this->assertEquals($this->images, $this->collection->getImages());
    }

    /**
     * @covers \PopularArtists\Image\Collection::getImagesAsArrays
     */
    public function testGetImagesAsArrays()
    {
        $expected = array();
        foreach ($this->images as $image) {
            $expected[$image->getSize()] = $image->toArray();
        }

        $this->assertEquals($expected, $this->collection->getImagesAsArrays());
    }
}
