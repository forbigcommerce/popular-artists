<?php

namespace PopularArtists\Tests\Artist;

use \PHPUnit\Framework\TestCase;
use \PopularArtists\Image\Image;
use \PopularArtists\Tests\Traits;

class ImageTest extends TestCase
{
    use Traits\ImageData;

    /**
     * @covers \PopularArtists\Image\Image::__construct
     */
    public function testConstructor()
    {
        $image = $this->getImage($this->getImageData());

        $this->assertInstanceOf(Image::class, $image);
    }

    /**
     * @covers \PopularArtists\Image\Image::getText
     */
    public function testGetText()
    {
        $data = $this->getImageData();
        $image = $this->getImage($data);

        $this->assertEquals($data[Image::TEXT], $image->getText());
    }

    /**
     * @covers \PopularArtists\Image\Image::getSize
     */
    public function testGetSize()
    {
        $data = $this->getImageData();
        $image = $this->getImage($data);

        $this->assertEquals($data[Image::SIZE], $image->getSize());
    }

    /**
     * @covers \PopularArtists\Image\Image::isSize
     */
    public function testIsSize()
    {
        $data = $this->getImageData(array(
            Image::SIZE => Image::SIZE_SMALL,
        ));
        $image = $this->getImage($data);

        $this->assertTrue($image->isSize(Image::SIZE_SMALL));
        $this->assertFalse($image->isSize(Image::SIZE_MEDIUM));
    }

    /**
     * @covers \PopularArtists\Image\Image::toArray
     */
    public function testToArray()
    {
        $data = $this->getImageData();
        $image = $this->getImage($data);

        $expected = $data;

        $this->assertEquals($expected, $image->toArray());
    }
}
