<?php

namespace PopularArtists\Tests\Artist;

use \PHPUnit\Framework\TestCase;
use \PopularArtists\Artist\Artist;
use \PopularArtists\Tests\Traits;

class ArtistTest extends TestCase
{
    use Traits\ArtistData;

    /**
     * @covers \PopularArtists\Artist\Artist::__construct
     */
    public function testConstructor()
    {
        $artist = $this->getArtist($this->getArtistData());

        $this->assertInstanceOf(Artist::class, $artist);
    }

    /**
     * @covers \PopularArtists\Artist\Artist::getName
     */
    public function testGetName()
    {
        $data = $this->getArtistData();
        $artist = $this->getArtist($data);

        $this->assertEquals($data[Artist::NAME], $artist->getName());
    }

    /**
     * @covers \PopularArtists\Artist\Artist::getListeners
     */
    public function testGetListeners()
    {
        $data = $this->getArtistData();
        $artist = $this->getArtist($data);

        $this->assertEquals($data[Artist::LISTENERS], $artist->getListeners());
    }

    /**
     * @covers \PopularArtists\Artist\Artist::getMbid
     */
    public function testGetMbid()
    {
        $data = $this->getArtistData();
        $artist = $this->getArtist($data);

        $this->assertEquals($data[Artist::MBID], $artist->getMbid());
    }

    /**
     * @covers \PopularArtists\Artist\Artist::getUrl
     */
    public function testGetUrl()
    {
        $data = $this->getArtistData();
        $artist = $this->getArtist($data);

        $this->assertEquals($data[Artist::URL], $artist->getUrl());
    }

    /**
     * @covers \PopularArtists\Artist\Artist::getStreamable
     */
    public function testGetStreamable()
    {
        $data = $this->getArtistData();
        $artist = $this->getArtist($data);

        $this->assertEquals($data[Artist::STREAMABLE], $artist->getStreamable());
    }

    /**
     * @covers \PopularArtists\Artist\Artist::getImages
     */
    public function testGetImages()
    {
        $data = $this->getArtistData();
        $artist = $this->getArtist($data);

        $this->assertEquals($data[Artist::IMAGES], $artist->getImages());
    }

    /**
     * @covers \PopularArtists\Artist\Artist::toArray
     */
    public function testToArray()
    {
        $data = $this->getArtistData();
        $artist = $this->getArtist($data);

        $expected = $data;
        $expected[Artist::IMAGES] = $data[Artist::IMAGES]->getImagesAsArrays();

        $this->assertEquals($expected, $artist->toArray());
    }
}
