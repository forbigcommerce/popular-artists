<?php

namespace PopularArtists\Tests\Artist;

use \PHPUnit\Framework\TestCase;
use \PopularArtists\Artist\Collection;
use \PopularArtists\Tests\Traits;

class CollectionTest extends TestCase
{
    use Traits\ArtistData;

    private $totalResults = 13;
    private $pageNumber = 1;
    private $collection;
    private $collectionVariadicArray;
    private $artists;

    public function setUp()
    {
        $artist = $this->getArtist($this->getArtistData());
        $artist2 = $this->getArtist($this->getArtistData());

        $this->artists = array($artist, $artist2);
        $this->collection = new Collection($this->totalResults, $this->pageNumber, $artist, $artist2);
        $this->collectionVariadicArray = new Collection($this->totalResults, $this->pageNumber, ...$this->artists);
    }

    /**
     * @covers \PopularArtists\Artist\Collection::__construct
     */
    public function testConstructor()
    {
        $this->assertInstanceOf(Collection::class, $this->collection);
        $this->assertInstanceOf(Collection::class, $this->collectionVariadicArray);
    }

    /**
     * @covers \PopularArtists\Artist\Collection::getTotalResults
     */
    public function testGetTotalResults()
    {
        $this->assertEquals($this->totalResults, $this->collection->getTotalResults());
    }

    /**
     * @covers \PopularArtists\Artist\Collection::getPageNumber
     */
    public function testGetPageNumber()
    {
        $this->assertEquals($this->pageNumber, $this->collection->getPageNumber());
    }

    /**
     * @covers \PopularArtists\Artist\Collection::getArtists
     */
    public function testGetArtists()
    {
        $this->assertEquals($this->artists, $this->collection->getArtists());
    }

    /**
     * @covers \PopularArtists\Artist\Collection::getArtistsAsArrays
     */
    public function testGetArtistsAsArrays()
    {
        $expected = array();
        foreach ($this->artists as $artist) {
            $expected[] = $artist->toArray();
        }

        $this->assertEquals($expected, $this->collection->getArtistsAsArrays());
    }
}
