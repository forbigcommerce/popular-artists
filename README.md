# popular-artists

Sample application for BigCommerce that retrieves popular artists from last.fm based on country searches.

# Installation

## Requirements

* ``docker``
* ``docker-compose``

``popular-artists`` is built using docker as a way of isolating development dependencies. This allows it to utilise a variety of technologies without pushing the burden of dependency management onto the developer.

``docker`` can be installed from https://docs.docker.com/engine/installation/.

Note that any installation path taken should also include ``docker-compose``

## Initial Setup

Once the requiremenst are installed, ``popular-artists`` can be setup by running

```
make init
```

This will build the various containers and install ``composer`` dependencies for you.

# Running ``popular-artists``

To start the application, run

```
make run
```

This will launch the containers for ``popular-artists`` and will make it accessible on the host at port 9000.

# Running unit tests and coverage metrics

Unit tests can be run with

```
make tests
```

Coverage metrics can be run with

```
make coverage
```

The reports will be available in HTML format at ``tests/reports``.

# Compromises/Shortcuts

* A nicer UI. While the current one is functional and more than just basic HTML, I usually prefer to make it actually neat and tidy (padding between images, better responsive nature, etc)
* Unit testing of the controllers. While I designed and implemented them to be unit testable, I ran out of time to actually do so.
* 404 and 500 etc error pages. Slim provides some basic stuff by itself, but who ever wants to see the default framework ones?
* Setting up my own fork of the handlebars view plugin in order to control the flags available. This reliance on a beta package wouldn't normally be my first choice, nor would pulling a package directly from GitHub (instead of packagist), however writing my own Handlebars view for Slim would have taken too much time and effort for this task.
* Cleanup (remove) Dockerfile.php-fpm. I originally thought I might need to tweak this container from the base but ended up not having to, so I would prefer to remove this entirely.
* Automated test running. Using something like CircleCI, Travis, etc. Anything that supports docker containers would have made this trivial as the docker-compose file I have would be usable there too.